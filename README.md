# OPM v2 Specification

A minimal implementation of Open iT Package Manager for C++.

## Install `opm`

```sh
pip install -e git+https://gitlab.com/openitdotcom/opm.git#egg=opm
opm --version
2.0.0
```

## Create a new project

```sh
mkdir myapp
cd myapp
opm init
```

This is the contents of the directory.

```
./include
./include/myapp.h
./opm.json
./src
./src/main.cpp
```

This is the content of `opm.json` file.

```json
{
    "name": "myapp",
    "description": "Add description here",
    "version": "0.1.0"
}
```

Install packages that will be used by this package.

```sh
opm install corelib filelib
```

An `/opm_packages` directory will be created. It contains the source of the libraries.

```
./include
./include/myapp.h
./opm.json
./opm_packages
./opm_packages/corelib
./opm_packages/filelib
./src
./src/main.cpp
```

> **Note:** The `/opm_packages` directory must not be committed. Add it to `.gitignore` file.

This is the updated content of `opm.json` file after `opm install`.

```json
{
    "name": "myapp",
    "description": "Add description here",
    "version": "0.1.0",
    "dependencies": [
        "filelib",
        "corelib"
    ]
}
```

To include the libraries in your program:

```cpp
#include <filelib>
#include <corelib>
```

To compile the package:

```sh
opm build
```

A `/build` folder is created that contains the `CMakeLists.txt` and the build files of Visual Studio or `gcc`.

```
./build
./build/CMakeLists.txt
./include
./include/myapp.h
./opm.json
./opm_packages
./opm_packages/corelib
./opm_packages/filelib
./src
./src/main.cpp
```

> **Note:** The `/build` directory must not be committed and is added to `.gitignore` file.

You can get the compiled binary inside the `/build` folder.

If you want to build using e.g. Visual Studio, you have to generate first instead of `opm build`.

> **Note:** The `opm build` also does `opm generate`.

```sh
opm generate
```

This will generate the solution files inside the `/build` folder.
Open the `/build/myapp.sln` to open Visual Studio.

## Repository

Let the user use `git` manually to add version control.
Upload the project in `https://gitlab.com/openitdotcom/<myapp>`.
But the design allows you to upload the repository in any location (even in different sites like GitHub).

If you create a library and want to add it to OPM package repository,
you have to upload it in:

```
https://gitlab.com/openitdotcom/opm/repositories/<mylibrary>
```

This allows your library to be used in other projects using `opm install <mylibrary>`.

## CI Pipeline

Let the user add any kind of CI pipeline manually, e.g. `.gitlab-ci.yml`.

## Integration

To add your new binary to the official Open iT installer,
upload the binaries to https://gitlab.com/openitdotcom/built.

This must be done in the CI pipeline (`.gitlab-ci.yml`).
The CI pipeline automatically uploads the binaries to the `built` repository.
When an installer is created, it will contain the latest binary automatically.
See this example project https://gitlab.com/openitdotcom/handlerd.

## Compile an existing project

```sh
git clone https://gitlab.com/openitdotcom/myapp
cd myapp
opm install
opm build
```

The `opm install` reads the `opm.json` file and downloads all the dependencies to the `/opm_packages` folder.
The `opm build` will compile the project inside the `/build` folder.
